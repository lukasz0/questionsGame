package com.pl.services;

import com.pl.exceptions.PlayerAlreadyExistsException;
import com.pl.repositories.PlayerRepository;
import com.pl.database.entities.PlayerEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PlayerService {

    private final PlayerRepository playerRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public void addPlayer(String name, String password) {
        if (playerRepository.findByNameIgnoreCase(name).isPresent()) {
            throw new PlayerAlreadyExistsException("Player already exsists");
        }
        PlayerEntity playerEntity = new PlayerEntity(name, passwordEncoder.encode(password));
        playerRepository.save(playerEntity);
    }
}
