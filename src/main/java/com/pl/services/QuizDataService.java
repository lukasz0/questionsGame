package com.pl.services;


import com.pl.database.entities.*;
import com.pl.dto.CategoryQuestionCountInfoDto;
import com.pl.dto.GameOptions;
import com.pl.dto.QuestionsDto;
import com.pl.exceptions.CategoryAlreadyExistsException;
import com.pl.exceptions.CategoryNotExistsException;
import com.pl.exceptions.NullCategoryException;
import com.pl.exceptions.QuestionAlreadyExistsException;
import com.pl.repositories.CategoryRepository;
import com.pl.repositories.GameRepository;
import com.pl.repositories.PlayerRepository;
import com.pl.repositories.QuestionRepository;
import com.pl.database.entities.*;
import com.pl.dto.CategoriesDto;
import com.pl.enumarated.Difficulty;
import lombok.NonNull;
import lombok.extern.java.Log;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log
public class QuizDataService {

    private final CategoryRepository categoryRepository;
    private final QuestionRepository questionRepository;
    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;

    public QuizDataService(CategoryRepository categoryRepository, QuestionRepository questionRepository, GameRepository gameRepository, PlayerRepository playerRepository) {
        this.categoryRepository = categoryRepository;
        this.questionRepository = questionRepository;
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
    }

    public List<CategoriesDto.CategoryDto> getQuizCategories() {
        RestTemplate restTemplate = new RestTemplate();
        CategoriesDto result = restTemplate.getForObject("https://opentdb.com/api_category.php", CategoriesDto.class);
        List<CategoryEntity> categoryEntities = categoryRepository.findAll();
        categoryEntities.stream()
                .forEach(categoryEntity -> result.addCategory(new CategoriesDto.CategoryDto(categoryEntity.getId(), categoryEntity.getName())));
        log.info("categories: " + result.getCategories());
        return result.getCategories();
    }

    @Transactional
    public List<CategoriesDto.CategoryDto> getQuizCategoryFromDB() {
        List<CategoryEntity> categoryEntities = categoryRepository.findAll();
        return categoryEntities.stream()
                .map(categoryEntity -> new CategoriesDto.CategoryDto(categoryEntity.getId(), categoryEntity.getName()))
                .collect(Collectors.toList());
    }

    public List<QuestionsDto.QuestionDto> getQuizQuestions(@NonNull GameOptions gameOptions) {
        CategoryQuestionCountInfoDto categoryQuestionCount = getCategoryQuestionCount(gameOptions.getCategoryId());
        int availableQuestionCount = categoryQuestionCount.getQuestionCountForDifficulty(gameOptions.getDifficulty());
        if (availableQuestionCount >= gameOptions.getNumberOfQuestions()) {
            return getQuizQuestions(gameOptions.getNumberOfQuestions(), gameOptions.getCategoryId(), gameOptions.getDifficulty());
        } else {
            return getQuizQuestions(availableQuestionCount, gameOptions.getCategoryId(), gameOptions.getDifficulty());
        }
    }

    @Transactional
    private List<QuestionsDto.QuestionDto> getQuizQuestions(int numberOfQuestions, int categoryId, Difficulty difficulty) {
        if (categoryRepository.hasExistsCategoryWithID((long) categoryId)) {
            final QuestionsDto result = new QuestionsDto();
            List<QuestionEntity> questionEntityList = questionRepository.findByCategoryIdQuantityQuestions(Integer.toString(categoryId), numberOfQuestions, difficulty.ordinal());

            questionEntityList.stream()
                    .forEach(questionEntity -> result.addQuestion(new QuestionsDto.QuestionDto(Integer.toString(categoryId),
                            questionEntity.getType(),
                            difficulty,
                            questionEntity.getQuestion(),
                            questionEntity.getAnswers().stream()//filtrujemy z odpowiedzi tylko poprawna odpowiedz i zwracamy jako string
                                    .filter(answerEntity -> answerEntity.isItCorrect()).findAny().orElseThrow().getAnswer(),
                            questionEntity.getAnswers().stream()
                                    .filter(answerEntity -> !answerEntity.isItCorrect())//filtrujemy z odpowiedzi tylko nieprawidłowe odpowiedzi  i zwracamy jako listę string
                                    .map(AnswerEntity::getAnswer)
                                    .collect(Collectors.toList()))));
            return result.getResults();
        } else {
            RestTemplate restTemplate = new RestTemplate();

            URI uri = UriComponentsBuilder.fromHttpUrl("https://opentdb.com/api.php?")
                    .queryParam("amount", numberOfQuestions)
                    .queryParam("category", categoryId)
                    .queryParam("difficulty", difficulty.name().toLowerCase())
                    .build().toUri();

            log.info("Get questions from: " + uri);

            QuestionsDto result = restTemplate.getForObject(uri, QuestionsDto.class);
            log.info("Quiz questions: " + result.getResults());
            return result.getResults();
        }

    }

    @Transactional
    private CategoryQuestionCountInfoDto getCategoryQuestionCount(int categoryId) {
        if (categoryRepository.hasExistsCategoryWithID((long) categoryId)) {
            CategoryQuestionCountInfoDto.CategoryQuestionCountDto countCategory = new CategoryQuestionCountInfoDto.CategoryQuestionCountDto(questionRepository.findByCategoryIdAndDifficulty(categoryId, Difficulty.easy.ordinal()), questionRepository.findByCategoryIdAndDifficulty(categoryId, Difficulty.medium.ordinal()), questionRepository.findByCategoryIdAndDifficulty(categoryId, Difficulty.hard.ordinal()));
            return new CategoryQuestionCountInfoDto(categoryId, countCategory);
        } else {
            RestTemplate restTemplate = new RestTemplate();
            URI uri = UriComponentsBuilder.fromHttpUrl("https://opentdb.com/api_count.php?")
                    .queryParam("category", categoryId)
                    .build().toUri();
            CategoryQuestionCountInfoDto result = restTemplate.getForObject(uri, CategoryQuestionCountInfoDto.class);
            log.info("Quiz category question count content: " + result);
            return result;
        }
    }

    @Transactional
    public CategoryEntity addCategory(String name) {
        CategoryEntity categoryEntity = new CategoryEntity(name);
        if (categoryRepository.existsCategoryEntityByName(name)) {
            throw new CategoryAlreadyExistsException("Category already exists:" + name);
        } else {
            categoryEntity = categoryRepository.save(categoryEntity);
        }
        return categoryEntity;
    }

    @Transactional
    public QuestionEntity addQuestion(QuestionsDto.QuestionDto questionDto) {
        log.info("Category od question" + questionDto.getCategory());
        List<AnswerEntity> answersEntity = questionDto.getIncorrectAnswers().stream()
                .map(incorrectAnswer -> new AnswerEntity(incorrectAnswer, false))
                .collect(Collectors.toList());
        answersEntity.add(new AnswerEntity(questionDto.getCorrectAnswer(), true));
        QuestionEntity questionEntity = new QuestionEntity(questionDto.getType(), questionDto.getDifficulty(), questionDto.getQuestion(), answersEntity);
        if (questionRepository.existsQuestionEntityByQuestion(questionEntity.getQuestion())) {
            throw new QuestionAlreadyExistsException("Question already exists:" + questionEntity.getQuestion());
        } else if (questionDto.getCategory() == null) {
            throw new NullCategoryException("Category is null");
        } else {
            questionEntity = questionRepository.saveAndFlush(questionEntity);
            log.info("questionEntity from QuizDataService: " + questionEntity);
            Optional<CategoryEntity> optionalCategoryEntity = categoryRepository.findById(Long.parseLong(questionDto.getCategory()));
            if (optionalCategoryEntity.isPresent()) {
                CategoryEntity categoryEntity = optionalCategoryEntity.orElseThrow();
                categoryEntity.addQuestion(questionEntity);
                categoryRepository.save(categoryEntity);
            } else {
                throw new CategoryNotExistsException("Category with id not exists: id:" + questionDto.getCategory());
            }
        }
        return questionEntity;
    }

    @Transactional
    public void addGame(Difficulty difficulty, int categoryId, int points, int totalQuestionNumber, LocalDateTime startGame, LocalDateTime endGame, List<QuestionsDto.QuestionDto> questionsWithCorrectAnswer, List<QuestionsDto.QuestionDto> questionsWithIncorrectAnswer) {
        //dodajemy grę tylko kategoria jest w BD - moze zmienic ze ma dodac do bazy danych
        if (categoryRepository.hasExistsCategoryWithID((long) categoryId)) {
            CategoryEntity categoryEntity = categoryRepository.findById((long) categoryId).get();
            GameEntity gameEntity = new GameEntity(difficulty, categoryEntity, points, totalQuestionNumber, startGame, endGame, mapToQuestionsEntity(questionsWithCorrectAnswer), mapToQuestionsEntity(questionsWithIncorrectAnswer));
            gameEntity = gameRepository.save(gameEntity);
            String playerName = SecurityContextHolder.getContext().getAuthentication().getName();
            log.info("Player name from add game: " + playerName);
            PlayerEntity playerEntity = playerRepository.findByNameIgnoreCase(playerName)
                    .orElseThrow(() -> new UsernameNotFoundException(playerName));
            playerEntity.addGame(gameEntity);
            playerRepository.save(playerEntity);
        }
    }

    private List<QuestionEntity> mapToQuestionsEntity(List<QuestionsDto.QuestionDto> questions) {
        return questions.stream()
                .map(questionDto -> mapToQuestionEntity(questionDto))
                .collect(Collectors.toList());
    }

    private QuestionEntity mapToQuestionEntity(QuestionsDto.QuestionDto questionDto) {
        List<AnswerEntity> answerEntities = new ArrayList<>();
        AnswerEntity answerEntity = new AnswerEntity(questionDto.getCorrectAnswer(), true);
        answerEntities.add(answerEntity);
        questionDto.getIncorrectAnswers().stream()
                .forEach(answer -> {
                    answerEntities.add(new AnswerEntity(answer, false));
                });
        return new QuestionEntity(questionDto.getType(), questionDto.getDifficulty(), questionDto.getQuestion(), answerEntities);
    }
}