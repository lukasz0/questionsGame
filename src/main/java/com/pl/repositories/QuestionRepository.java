package com.pl.repositories;

import com.pl.database.entities.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<QuestionEntity,Long> {

    @Query(value ="select * from questions q inner join categories c on q.category_id = c.id where q.category_id = ?1 and q.difficulty=?3 ORDER BY RANDOM() limit ?2", nativeQuery = true)
    List<QuestionEntity> findByCategoryIdQuantityQuestions(String categoryId, int numberOfQuestions, int difficulty);

    @Query(value ="select count(q.id_question) from questions q inner join categories c on q.category_id = c.id where q.category_id = ?1 and q.difficulty = ?2", nativeQuery = true)
    int findByCategoryIdAndDifficulty(int categoryId, int difficulty);

    boolean existsQuestionEntityByQuestion(String question);
}
