package com.pl.database.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@ToString
@Table(name="answers")
@RequiredArgsConstructor
public class AnswerEntity {

    @Id
    @GeneratedValue
    private Long idAnswer;
    private String answer;

    private boolean isItCorrect;

    public AnswerEntity(String answer, boolean isItCorrect) {
        this.answer = answer;
        this.isItCorrect = isItCorrect;
    }
}
