package com.pl.database.entities;

import com.pl.enumarated.Difficulty;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


@Getter
@ToString
@Entity
@Table(name="games")
@RequiredArgsConstructor
public class GameEntity {

    @Id
    @GeneratedValue
    private Long id;

    private Difficulty difficulty;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="category_id")
    private CategoryEntity categoryEntity;

    private int result;
    private int totalQuestionNumber;

    private LocalDateTime startGame;
    private LocalDateTime endGame;

//    private TypeGame typeGame;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<QuestionEntity> questionsWithCorrectAnswer;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<QuestionEntity> questionsWithIncorrectAnswer;

    public GameEntity(Difficulty difficulty, CategoryEntity categoryEntity, int result, int totalQuestionNumber, LocalDateTime startGame, LocalDateTime endGame, List<QuestionEntity> questionsWithCorrectAnswer, List<QuestionEntity> questionsWithIncorrectAnswer) {
        this.difficulty = difficulty;
        this.categoryEntity = categoryEntity;
        this.result = result;
        this.totalQuestionNumber = totalQuestionNumber;
        this.startGame = startGame;
        this.endGame = endGame;
        this.questionsWithCorrectAnswer = questionsWithCorrectAnswer;
        this.questionsWithIncorrectAnswer = questionsWithIncorrectAnswer;
    }
}
