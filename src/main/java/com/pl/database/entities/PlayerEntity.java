package com.pl.database.entities;


import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//bezparametrowy konstruktr wymagany przez JPA
@NoArgsConstructor
@Getter
@Entity(name = "players")
public class PlayerEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;
    private String password;

    //adnotacja - hibernate stworzy nową tabelę w bazie danych
    //eager -  przy każdym pobieraniu playerentity pobieramy roles, jeżeli byłoby puste to domyślnie jest lazy i nie pobierze roles za każdym razem pobierania playerEntity
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> roles = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "player_id")
    private List<GameEntity> games = new ArrayList<>();


    public PlayerEntity(String name, String password) {
        this.name = name;
        this.password = password;
        this.games = new ArrayList<>();
        //TODO to change
        this.roles = Set.of("ROLE_ADMIN");
    }

    @Override
    public String toString() {
        return "PlayerEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public void addGame(GameEntity gameEntity) {
        this.games.add(gameEntity);
    }
}
