package com.pl.dto;

import com.pl.enumarated.Difficulty;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

//przykładowy json
//{"category_id":10,"category_question_count":{"total_question_count":97,"total_easy_question_count":31,"total_medium_question_count":40,"total_hard_question_count":26}}

@NoArgsConstructor
@ToString
public class CategoryQuestionCountInfoDto {

    @JsonProperty("category_id")
    private int categoryId;

    @JsonProperty("category_question_count")
    private CategoryQuestionCountDto categoryQuestionCountDto;

    public CategoryQuestionCountInfoDto(int categoryId, CategoryQuestionCountDto categoryQuestionCountDto) {
        this.categoryId = categoryId;
        this.categoryQuestionCountDto = categoryQuestionCountDto;
    }

    public int getTotalQuestionCount(){
        return categoryQuestionCountDto.totalQuestionCount;
    }

    public int getQuestionCountForDifficulty(Difficulty difficulty){
        switch(difficulty){
            case easy:
                return categoryQuestionCountDto.totalEasyQuestionCount;
            case medium:
                return categoryQuestionCountDto.totalMediumQuestionCount;
            case hard:
                return categoryQuestionCountDto.totalHardQuestionCount;
        }
        return 0;
    }

    @Getter
    @NoArgsConstructor
    @ToString
    public static class CategoryQuestionCountDto {


        @JsonProperty("total_question_count")
        private int totalQuestionCount;
        @JsonProperty("total_easy_question_count")
        private int totalEasyQuestionCount;
        @JsonProperty("total_medium_question_count")
        private int totalMediumQuestionCount;
        @JsonProperty("total_hard_question_count")
        private int totalHardQuestionCount;

        public CategoryQuestionCountDto(int totalEasyQuestionCount, int totalMediumQuestionCount, int totalHardQuestionCount) {
            this.totalQuestionCount = totalEasyQuestionCount+totalMediumQuestionCount+totalHardQuestionCount;
            this.totalEasyQuestionCount = totalEasyQuestionCount;
            this.totalMediumQuestionCount = totalMediumQuestionCount;
            this.totalHardQuestionCount = totalHardQuestionCount;
        }
    }
}
