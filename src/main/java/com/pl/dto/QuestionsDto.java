package com.pl.dto;

import com.pl.enumarated.Difficulty;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.web.util.HtmlUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@ToString
@Data
public class QuestionsDto {

    @JsonProperty("response_code")
    private int responseCode;
    private List<QuestionDto> results=new ArrayList<>();

    public void addQuestion(QuestionDto questionDto){
        this.results.add(questionDto);
    }

    @NoArgsConstructor
    @Getter
    @ToString
    @Data
    public static class QuestionDto{

        private String category;
        private String type;
        private Difficulty difficulty;
        private String question;

        @JsonProperty("correct_answer")//dzięki tej adnotacji wiadomo że to co nazywa się w json correct_answer zostanie w javie pobrane do pola correctAnswer
        private String correctAnswer;

        @JsonProperty("incorrect_answers")
        private List<String> incorrectAnswers;

        public QuestionDto(String category, String type, Difficulty difficulty, String question, String correctAnswer, List<String> incorrectAnswers) {
            this.category = category;
            this.type = type;
            this.difficulty = difficulty;
            this.question = question;
            this.correctAnswer = correctAnswer;
            this.incorrectAnswers = incorrectAnswers;
        }

        //HtmlUtils.htmlUnescape - używamy tej metody aby nie było  tzw. krzaków. Wszystkie nistandardowe znaki na Trivia DB są zamieniane na kod znaków w HTML. Aby odkodować używamy metody HtmlUtils.htmlUnescape. Jackson zamieniający json na obiekty javove korzysta z setter jeżeli są w klasie zapisane. A więc przy pobraniu danych z zewnątrz zostana one odrazu odkodowane do UNICODE
        public QuestionDto setQuestion(String question) {
            this.question = HtmlUtils.htmlUnescape(question);
            return this;
        }

        public QuestionDto setCorrectAnswer(String correctAnswer) {
            this.correctAnswer = HtmlUtils.htmlUnescape(correctAnswer);
            return this;
        }

        public QuestionDto setIncorrectAnswers(List<String> incorrectAnswers) {
            this.incorrectAnswers = incorrectAnswers.stream()
            .map(answer->HtmlUtils.htmlUnescape(answer))
            .collect(Collectors.toList());
            return this;
        }
    }
}
