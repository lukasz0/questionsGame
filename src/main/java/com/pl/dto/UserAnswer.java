package com.pl.dto;

import lombok.Data;

@Data
public class UserAnswer {
    private String answer;
}
