package com.pl.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Getter
@ToString
public class CategoriesDto {

    @JsonProperty("trivia_categories")
//dzięki tej adnotacji wiadomo że to co nazywa się w json trivia_categories zostanie w javie pobrane do pola categories
    private List<CategoryDto> categories;

    public void addCategory(CategoryDto categoryDto) {
        this.categories.add(categoryDto);
    }

    @NoArgsConstructor
    @Getter
    @ToString
    public static class CategoryDto {
        private Long id;
        private String name;

        public CategoryDto(Long id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
