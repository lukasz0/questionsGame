package com.pl.exceptions;


public class PlayerAlreadyExistsException extends RuntimeException {
    public PlayerAlreadyExistsException(String message) {
        super(message);
    }
    public PlayerAlreadyExistsException(String message, Throwable cause){
        super(message, cause);
    }
}
