package com.pl.exceptions;

public class QuestionAlreadyExistsException extends RuntimeException {
    public QuestionAlreadyExistsException(String message) {
        super(message);
    }
    public QuestionAlreadyExistsException(String message, Throwable cause){
        super(message, cause);
    }
}
