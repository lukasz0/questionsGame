package com.pl.controllers;

//import com.dto.*;
import com.pl.dto.GameOptions;
import com.pl.dto.PlayerDto;
import com.pl.dto.QuestionsDto;
import com.pl.dto.UserAnswer;
import com.pl.exceptions.CategoryAlreadyExistsException;
import com.pl.exceptions.GameOptionsNotExistsException;
import com.pl.exceptions.NullCategoryException;
import com.pl.exceptions.QuestionAlreadyExistsException;
import com.pl.services.OngoingGameService;
import com.pl.services.PlayerService;
import com.pl.services.QuizDataService;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Log
@org.springframework.stereotype.Controller
public class RestController {

    private final QuizDataService quizDataService;
    private final OngoingGameService ongoingGameService;
    private final PlayerService playerService;

    public RestController(QuizDataService quizDataService, OngoingGameService ongoingGameService, PlayerService playerService) {
        this.quizDataService = quizDataService;
        this.ongoingGameService = ongoingGameService;
        this.playerService = playerService;
    }

    @GetMapping("/select")
    public String select(Model model) {
        model.addAttribute("gameOptions", new GameOptions());
        model.addAttribute("categories", quizDataService.getQuizCategories());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getName());
        return "select";
    }

    @PostMapping("/select")
    public String postSelectForm(Model model, @ModelAttribute GameOptions gameOptions) {
        log.info("Form submitted with data: " + gameOptions);
        ongoingGameService.init(gameOptions);
        return "redirect:game";
    }

    @GetMapping("/game")
    public String game(Model model) {
        model.addAttribute("userAnswer", new UserAnswer());
        model.addAttribute("currentQuestionNumber", ongoingGameService.getCurrentQuestionNumber());
        model.addAttribute("totalQuestionNumber", ongoingGameService.getTotalQuestionNumber());
        model.addAttribute("currentQuestion", ongoingGameService.getCurrentQuestion());
        model.addAttribute("currentQuestionAnswers", ongoingGameService.getCurrentQuestionAnswersInRandomOrder());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getName());
        return "game";
    }

    @PostMapping("/game")
    public String postSelectForm(Model model, @ModelAttribute UserAnswer userAnswer) {
        ongoingGameService.checkAnswerFromCurrentQuestionAndUpdatePoints(userAnswer.getAnswer());
        boolean hasNextQuestion = ongoingGameService.proceedToNextQuestion();
        if (hasNextQuestion) {
            return "redirect:game";
        } else {

            return "redirect:summary";
        }
    }

    @GetMapping("/summary")
    public String summary(Model model) {
        model.addAttribute("difficulty", ongoingGameService.getDifficulty());
        model.addAttribute("categoryName", ongoingGameService.getCategoryName());
        model.addAttribute("points", ongoingGameService.getPoints());
        model.addAttribute("maxPoints", ongoingGameService.getTotalQuestionNumber());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getName());
        return "summary";
    }

    //TODO: to change
    @ExceptionHandler({CategoryAlreadyExistsException.class, QuestionAlreadyExistsException.class, NullCategoryException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleError() {
        return "Wystąpił błąd dodawania kategorii lub pytania";
    }

    @ExceptionHandler(GameOptionsNotExistsException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleGameOptionsNotExistsException() {
        return "Nie zostały wypełnione wszystkie wymagane pola";
    }


    @GetMapping("/addquestion")
    public String addQuestion(Model model) {
        model.addAttribute("userQuestion", new QuestionsDto.QuestionDto());
        model.addAttribute("categories", quizDataService.getQuizCategoryFromDB());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getName());
        return "addquestion";
    }

    @PostMapping("/addquestion")
    public String addQuestionForm(Model model, @ModelAttribute QuestionsDto.QuestionDto userQuestion) {
        log.info("Form submitted with data: " + userQuestion);
        quizDataService.addQuestion(userQuestion);
        return "index";
    }

    @GetMapping("/addplayer")
    public String addPlayer(Model model) {
        model.addAttribute("player", new PlayerDto());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getName());
        return "addplayer";
    }

    @PostMapping("/addplayer")
    public String addPlayerForm(Model model, @ModelAttribute PlayerDto player) {
        playerService.addPlayer(player.getName(), player.getPassword());
        return "index";
    }

    @GetMapping("/index")
    public String start(Model model) {
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getName());
        return "index";
    }
}
