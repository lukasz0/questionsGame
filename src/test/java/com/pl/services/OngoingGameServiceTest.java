package com.pl.services;

import com.pl.dto.GameOptions;
import com.pl.enumarated.Difficulty;
import com.pl.exceptions.GameOptionsNotExistsException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OngoingGameServiceTest {

    @Autowired
    private OngoingGameService ongoingGameService;

    @Test
    void shouldInit() {
        //given
        GameOptions gameOptions = new GameOptions(2, Difficulty.medium, 9);

        //when
        ongoingGameService.init(gameOptions);

        //then
        assertAll(
                () -> assertEquals(1, ongoingGameService.getCurrentQuestionNumber()),
                () -> assertEquals(0, ongoingGameService.getPoints()),
                () -> assertEquals(Difficulty.medium, ongoingGameService.getDifficulty()),
                () -> assertNotNull(ongoingGameService.getCategoryName()),
                () -> assertEquals(2, ongoingGameService.getTotalQuestionNumber())
        );
    }

    @Test
    void throwsGameOptionsNotExistsExceptionOnAddingNullGameOptions() {

        //Then
        assertThrows(GameOptionsNotExistsException.class, () -> ongoingGameService.init(null));
    }

    @Test
    void throwsGameOptionsNotExistsExceptionOnAddingGameOptionsWithNullDifficulty() {
        //Given
        GameOptions gameOptions = new GameOptions(3, null, 9);

        //Then
        assertThrows(GameOptionsNotExistsException.class, () -> ongoingGameService.init(gameOptions));
    }

}