package com.pl.services;

import com.pl.dto.GameOptions;
import com.pl.dto.QuestionsDto;
import com.pl.dto.CategoriesDto;
import com.pl.enumarated.Difficulty;
import com.pl.exceptions.CategoryAlreadyExistsException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class QuizDataServiceTest {

    @Autowired
    private QuizDataService quizDataService;

    @Test
    void shouldGetQuizCategories() {

        //When
        List<CategoriesDto.CategoryDto> result = quizDataService.getQuizCategories();

        //Then
        assertNotNull(result);
    }

    @Test
    void shouldGetQuizCategoryFromDB() {

        //When
        List<CategoriesDto.CategoryDto> result = quizDataService.getQuizCategoryFromDB();

        //Then
        assertNotNull(result);
    }

    @Test
    void shouldGetQuestionsWhenGivenNumberOfQuestion10000() {
        //Given
        GameOptions gameOptions = new GameOptions(10000, Difficulty.easy, 9);

        //When
        List<QuestionsDto.QuestionDto> result = quizDataService.getQuizQuestions(gameOptions);

        //Then
        assertAll(
                () -> assertNotEquals(10000, result.size()),
                () -> assertNotNull(result)
        );
    }


    @Test
    void shouldGet4QuestionsWhenGivenNumberOfQuestion4() {
        //Given
        GameOptions gameOptions = new GameOptions(4, Difficulty.easy, 9);

        //When
        List<QuestionsDto.QuestionDto> result = quizDataService.getQuizQuestions(gameOptions);

        //Then
        assertEquals(4, result.size());
    }


    @Test
    void throwsCategoryAlreadyExistsExceptionOnAddingCategoryQuestionsFromDB() {

        //Then
        assertThrows(CategoryAlreadyExistsException.class, () -> quizDataService.addCategory("Mathemastics: Questions from DB"));
    }
}